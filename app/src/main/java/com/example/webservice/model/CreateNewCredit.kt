package com.example.webservice.model

data class CreateNewCredit(
    val status: Int,
    val message: String,
    val credit: Credit?
)