package com.example.webservice.model

data class CreditDataClass(
        val status: Int?,
        val message: String?,
        val credit: List<Operaciones>,
)