package com.example.webservice.model

import com.squareup.moshi.Json

data class Credit (
        val nombre_ac: String,
        val clientes: Int,
        val monto: Int,
        val idbanco: Int,
        val fecha_desembolso: String,
        val idsucursal: Int,
        val idregional: Int
    )

data class Operaciones (
        @Json (name = "id") val id: Int?,
        @Json (name = "nombre_ac") val nombre_ac: String,
        @Json (name = "clientes") val clientes: Int,
        @Json (name = "monto") val monto: Int,
        @Json (name = "nombrebanco") val nombrebanco: String,
        @Json (name = "fecha_desembolso") val fecha_desembolso: String,
        @Json (name = "nombresucursal") val nombresucursal: String,
        @Json (name = "nombreregional") val nombreregional: String
)