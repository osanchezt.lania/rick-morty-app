package com.example.webservice.network

import com.example.webservice.model.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

//private const val BASE_URL = "https://jsonplaceholder.typicode.com/"
//private const val BASE_URL = "https://app-operaciones.herokuapp.com/"
//private const val BASE_URL = "http://192.168.1.67:4000/"
private const val BASE_URL = "https://rickandmortyapi.com/"

private val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/*private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

 */

interface ApiService {

    @GET("posts/{id}")
    suspend fun getPostById(@Path("id") id: Int): Response<Post>

    @POST("posts")
    suspend fun createPost(@Body post: Post): Response<Post>

    @GET("operaciones")
    suspend fun getOperaciones(): List<Operaciones>

    @GET("operaciones/{id}")
    suspend fun getOperacionesById(@Path("id") id: Int): Response<CreditDataClass>

    @POST("operaciones")
    suspend fun createCredit(@Body credit: Credit): Response<CreateNewCredit>

    @PUT("operaciones/{id}")
    suspend fun updateCredit(
        @Path("id") id: Int,
        @Body credit: Credit
    ): Response<CreateNewCredit>

    @DELETE("operaciones/{id}")
    suspend fun deleteCredito(@Path("id")id: Int): Response<CreateNewCredit>


    @GET("api/character")
    suspend fun getCharacters(
        @Query("page") page: Int
    ): Response<Characters>

    @GET("api/character")
    suspend fun getCharactersByStatus(
        @Query("status") status: String
    ): Response<Characters>

    @GET("api/character")
    suspend fun getCharactersByGender(
        @Query("gender") gender: String
    ): Response<Characters>
}

object Api{
    val retrofitService:ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}