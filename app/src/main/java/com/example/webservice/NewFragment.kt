package com.example.webservice

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.webservice.adapter.CustomAdapter
import com.example.webservice.databinding.FragmentNewBinding
import com.example.webservice.model.Results
import com.example.webservice.viewModel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog

const val TAG = "NewFragment"
class NewFragment : Fragment(), CustomAdapter.ItemClickListener {

    private lateinit var binding: FragmentNewBinding
    private lateinit var gender: String
    private val mainViewModel: MainViewModel by viewModels()
    private lateinit var adapter : CustomAdapter
    private var characters: List<Results> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate Called")
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView Called")
        binding = FragmentNewBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.lifecycleOwner = this
        binding.mainViewModel = mainViewModel
        initRecyclerView(binding.root)
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated Called")
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        view.findViewById<Toolbar>(R.id.toolbar)
            .setupWithNavController(navController, appBarConfiguration)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        Log.d(TAG, "onStart Called")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume Called")
        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "onPause Called")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop Called")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d(TAG, "onDestroyView Called")
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy Called")
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.new_fragment_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.findByGenderMenu -> {
                val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
                val view = layoutInflater.inflate(R.layout.bottom_sheet_new_fragment, null)
                bottomSheetDialog.setContentView(view)
                bottomSheetDialog.show()
                characters = listOf()
                adapter.setData(characters)
                mainViewModel.resetData()
                view.findViewById<Button>(R.id.btn_searchByGender).setOnClickListener {
                    gender = when (view.findViewById<RadioGroup>(R.id.gender_options).checkedRadioButtonId){
                        R.id.female_option -> "female"
                        R.id.male_option -> "male"
                        R.id.genderless_option -> "genderless "
                        else -> "unknown"
                    }
                    loadCharactersByGender()
                    bottomSheetDialog.dismiss()
                }
                true
            }
        else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.new_fragment_recycler_view)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        adapter = CustomAdapter(characters, this)
        recyclerView.adapter = adapter
    }

    private fun loadCharactersByGender() {
        mainViewModel.resetData()
        mainViewModel.loadCharactersByGender(gender)
        mainViewModel.getCharactersByGender.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) {
                //Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            } else {
                characters =  response.body()!!.results
                adapter.setData(characters)
                Log.d("Response", response.toString())
            }
        })
    }

    override fun onItemClick(results: Results) {
        Toast.makeText(context, results.name, Toast.LENGTH_SHORT).show()
    }

}