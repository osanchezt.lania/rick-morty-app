package com.example.webservice.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.webservice.R
import com.example.webservice.model.Results
import de.hdodenhof.circleimageview.CircleImageView

class CustomAdapter(
    var characters: List<Results>,
    val itemClickListener: ItemClickListener
): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private var characterList = listOf<Results>()

    interface ItemClickListener {
        fun onItemClick(results: Results)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: CircleImageView
        val name: TextView

        init {
            imageView = view.findViewById(R.id.item_imageView)
            name =  view.findViewById(R.id.item_name)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.load(characterList[position].image)
        holder.name.text = characterList[position].name
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(characterList[position])
        }
    }

    override fun getItemCount() = characterList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newList: List<Results>) {
        characterList = newList
        notifyDataSetChanged()
    }

}