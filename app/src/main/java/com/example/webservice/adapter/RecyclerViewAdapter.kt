package com.example.webservice.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.webservice.R
import com.example.webservice.model.Results

class RecyclerViewAdapter(
    val itemClickListener : ItemClickListener
) : PagingDataAdapter<Results, RecyclerViewAdapter.MyViewHolder>(DiffUtilCallBack()) {

    interface ItemClickListener {
        fun onItemClick(results: Results)
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.MyViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(getItem(position)!!)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerViewAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)

        return MyViewHolder(view)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemImageView: ImageView = view.findViewById(R.id.item_imageView)
        val itemName: TextView = view.findViewById(R.id.item_name)

        fun bind(character: Results) {
            itemImageView.load(character.image)
            itemName.text = character.name
        }
    }

    class DiffUtilCallBack : DiffUtil.ItemCallback<Results>() {
        override fun areItemsTheSame(oldItem: Results, newItem: Results): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Results, newItem: Results): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.species == newItem.species
        }

    }
}