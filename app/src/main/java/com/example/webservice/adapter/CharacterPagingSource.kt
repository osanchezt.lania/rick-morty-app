package com.example.webservice.adapter

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.webservice.model.Results
import com.example.webservice.network.Api
import com.example.webservice.network.ApiService
import java.lang.Exception

class CharacterPagingSource(): PagingSource<Int, Results>() {
    override fun getRefreshKey(state: PagingState<Int, Results>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Results> {
        return try {
            val nextPage: Int = params.key ?: FIRST_PAGE_INDEX
            val response = Api.retrofitService.getCharacters(nextPage)
            var netPageNumber: Int? = null

            if (response.body()?.info?.next != null) {
                val uri = Uri.parse(response.body()?.info?.next!!)
                val nextPageQuery = uri.getQueryParameter("page")
                netPageNumber = nextPageQuery?.toInt()
            }

            LoadResult.Page(
                data = response.body()?.results!!,
                //prevKey = null,
                prevKey = if (nextPage == FIRST_PAGE_INDEX) null else nextPage - 1,
                //nextKey = netPageNumber
                nextKey = if (response.body()?.results?.isEmpty()!!) null else nextPage + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }

}