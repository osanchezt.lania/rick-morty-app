package com.example.webservice

import android.opengl.Visibility
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.webservice.viewModel.CreditApiStatus
import de.hdodenhof.circleimageview.CircleImageView

@BindingAdapter("apiStatus")
fun bindStatus(progressBar: ProgressBar, status: CreditApiStatus?) {
    when (status) {
        CreditApiStatus.LOADING -> { progressBar.visibility = View.VISIBLE }
        CreditApiStatus.DONE -> { progressBar.visibility = View.GONE }
        CreditApiStatus.ERROR -> { progressBar.visibility = View.GONE }
    }
}

@BindingAdapter("connectionStatus")
fun bindConnectionStatus(statusImageView: ImageView, status: CreditApiStatus?) {
    if (status == CreditApiStatus.ERROR) {
        statusImageView.visibility = View.VISIBLE
        statusImageView.setImageResource(R.drawable.ic_connection_error)
    } else {
        statusImageView.visibility = View.GONE
    }
}

@BindingAdapter("messageConnection")
fun bindMessageConnection(textView: TextView, status: CreditApiStatus?) {
    if (status == CreditApiStatus.ERROR) {
        textView.visibility = View.VISIBLE
    } else {
        textView.visibility = View.INVISIBLE
    }
}