package com.example.webservice.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.webservice.adapter.CharacterPagingSource
import com.example.webservice.model.*
import com.example.webservice.network.Api
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

enum class CreditApiStatus { LOADING, DONE, ERROR }

class MainViewModel: ViewModel() {

    fun getListData() : Flow<PagingData<Results>> {
        return Pager (
            config = PagingConfig(pageSize = 42, maxSize = 826),
            pagingSourceFactory = { CharacterPagingSource() }
                ).flow.cachedIn(viewModelScope)
    }

    //Get POST by id
    var _getPost = MutableLiveData<Response<Post>?>()
    val getPost : LiveData<Response<Post>?> = _getPost

    //GET Characters Rick and Morty API
    private val _getCharacters = MutableLiveData<Response<Characters>?>()
    val getCharacters : LiveData<Response<Characters>?> = _getCharacters

    //GET Characters by Status Rick and Morty API
    private val _getCharactersByStatus = MutableLiveData<Response<Characters>?>()
    val getCharactersByStatus : LiveData<Response<Characters>?> = _getCharactersByStatus

    //GET Characters by Gender Rick and Morty API
    private val _getCharactersByGender = MutableLiveData<Response<Characters>?>()
    val getCharactersByGender : LiveData<Response<Characters>?> = _getCharactersByGender

    //create a new POST
    var _response = MutableLiveData<Response<Post>>()

    //API response/message
    private var _message = MutableLiveData<String?>()
    val message : LiveData<String?> = _message

    //Get credits
    private val _credits = MutableLiveData<List<Operaciones>?>()
    val credits : LiveData<List<Operaciones>?> = _credits

    //Get credit by id
    var _getCredit = MutableLiveData<Response<CreditDataClass>?>()

    //Create a new credit
    var _credit = MutableLiveData<Response<CreateNewCredit>?>()
    val _messageCredit = MutableLiveData<String>()

    //Update credit
    private val _creditUpdated = MutableLiveData<Response<CreateNewCredit>?>()
    val creditUpdated: LiveData<Response<CreateNewCredit>?> = _creditUpdated

    //Delete Credit
    var _deleteCredit = MutableLiveData<Response<CreateNewCredit>?>()

    //Variables status ProgressBar
    private val _status = MutableLiveData<CreditApiStatus>()
    val status: LiveData<CreditApiStatus> = _status

    init {

    }

    //Corrutine to GET Rick and Morty API characters
    fun loadCharacters(page: Int) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            try {
                val result = Api.retrofitService.getCharacters(page)
                _getCharacters.value = result
                _status.value = CreditApiStatus.DONE
                _message.value = "Done"
            } catch (e: Exception) {
                _status.value = CreditApiStatus.ERROR
                _getCharacters.value = null
                _message.value = "${e.message}"
            }
        }
    }

    fun resetData() {
        _getCharacters.value = null
        _getCharactersByGender.value = null
        _getCharactersByStatus.value = null
        //_message.value = "Loading..."
    }

    //Corrutine to GET characters by status Rick and Morty API
    fun loadCharactersByStatus(status: String) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            try {
                val result = Api.retrofitService.getCharactersByStatus(status)
                _getCharactersByStatus.value = result
                _status.value = CreditApiStatus.DONE
            } catch (e: Exception) {
                _status.value = CreditApiStatus.ERROR
                _getCharactersByStatus.value = null
                _message.value = "${e.message}"
            }
        }
    }

    //Corrutine to GET characters by gender Rick and Morty API
    fun loadCharactersByGender(gender: String) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            try {
                val result = Api.retrofitService.getCharactersByGender(gender)
                _getCharactersByGender.value = result
                _status.value = CreditApiStatus.DONE
                //_message.value = null
            } catch (e: Exception) {
                _status.value = CreditApiStatus.ERROR
                _getCharactersByGender.value = null
                _message.value = "${e.message}"
            }
        }
    }

    //Corrutine to get data (Credits)
    fun loadData() {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            try {
                val result = Api.retrofitService.getOperaciones()
                _credits.value = result
                _message.value = "${result}"
                _status.value = CreditApiStatus.DONE
            } catch (e: Exception) {
                _credits.value = null
                _message.value = "${e.message}"
                _status.value = CreditApiStatus.ERROR
            }
        }
    }

    //Corrutine to get a credit by id
    fun getCredit(id: Int) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            delay(2000)
            try {
                //val response = Api.retrofitService.getOperacionesById(id)
                val response = Api.retrofitService.getOperacionesById(id)
                _getCredit.value = response
                _status.value = CreditApiStatus.DONE
                //_messageCredit.value = "Done"
            } catch (e: Exception) {
                _getCredit.value = null
                _status.value = CreditApiStatus.ERROR
                _messageCredit.value = "${e.message}"
            }
        }
    }

    //Corrutine to create a new Credit
    fun createNewCredit(credit: Credit) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            delay(2000)
            try {
                val response = Api.retrofitService.createCredit(credit)
                _credit.value = response
                _status.value = CreditApiStatus.DONE
                //_messageCredit.value = "Done"
            }catch (e: Exception) {
                _credit.value = null
                _status.value = CreditApiStatus.ERROR
                _messageCredit.value = "${e.message}"
            }
        }
    }

    //Corrutine to update a credit
    fun updateCredit(id: Int, credit: Credit) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            delay(2000)
            try {
                val response = Api.retrofitService.updateCredit(id, credit)
                _creditUpdated.value = response
                _status.value = CreditApiStatus.DONE
            } catch (e:Exception) {
                _creditUpdated.value = null
                _status.value = CreditApiStatus.ERROR
                _message.value = "${e.message}"
            }
        }
    }

    //Corrutine to delete a credit
    fun deleteCredit(id: Int) {
        viewModelScope.launch {
            _status.value = CreditApiStatus.LOADING
            delay(2000)
            try {
                val result = Api.retrofitService.deleteCredito(id)
                _deleteCredit.value = result
                _status.value = CreditApiStatus.DONE
            } catch (e: Exception) {
                _deleteCredit.value = null
                _status.value = CreditApiStatus.ERROR
                _messageCredit.value = "${e.message}"
            }
        }
    }



    //
    fun getPost(id: Int) {
        viewModelScope.launch {
            try {
                val response = Api.retrofitService.getPostById(id)
                _getPost.value = response
            } catch (e: Exception) {
                _getPost.value = null
                _message.value = "${e.message}"
            }
        }
    }

    fun createNewPost(post: Post) {
        viewModelScope.launch {
            try {
                val response = Api.retrofitService.createPost(post)
                _response.value = response
                _message.value = "Post created successfully"
            } catch (e: Exception) {
                _message.value = "${e.message}"
            }
        }
    }

    public override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}