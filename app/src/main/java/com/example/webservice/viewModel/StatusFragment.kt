package com.example.webservice.viewModel

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.webservice.R
import com.example.webservice.adapter.CustomAdapter
import com.example.webservice.databinding.FragmentStatusBinding
import com.example.webservice.model.Results
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar

private const val Tag = "StatusFragment"
class StatusFragment : Fragment(), CustomAdapter.ItemClickListener {

    private var _binding: FragmentStatusBinding? = null
    val binding get() = _binding!!
    private val mainViewModel: MainViewModel by viewModels()
    private lateinit var adapter : CustomAdapter
    private var characters: List<Results> = listOf()
    private lateinit var status: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStatusBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.lifecycleOwner = this
        binding.mainViewModel = mainViewModel
        initRecyclerView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        view.findViewById<Toolbar>(R.id.toolbar)
            .setupWithNavController(navController, appBarConfiguration)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.status_fragment_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.findByStatusMenu -> {
                val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
                val view = layoutInflater.inflate(R.layout.layout_bottom_sheet, null)
                bottomSheetDialog.setContentView(view)
                bottomSheetDialog.show()
                characters = listOf()
                adapter.setData(characters)
                mainViewModel.resetData()
                view.findViewById<Button>(R.id.searchButton).setOnClickListener {
                    status = when (view.findViewById<RadioGroup>(R.id.status_options).checkedRadioButtonId){
                        R.id.alive_option -> "alive"
                        R.id.dead_option -> "dead"
                        else -> "unknown"
                    }
                    loadCharactersByStatus()
                    bottomSheetDialog.dismiss()
                }
                true
            }
        else -> super.onOptionsItemSelected(item)
        }
    }

    private fun loadCharactersByStatus() {
        mainViewModel.loadCharactersByStatus(status)
        mainViewModel.getCharactersByStatus.observe(viewLifecycleOwner, Observer { response ->
            if (response != null) {
                characters =  response.body()!!.results
                adapter.setData(characters)
            }
        })
    }

    private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.status_fragment_recycler_view)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        adapter = CustomAdapter(characters, this)
        recyclerView.adapter = adapter
    }

    override fun onItemClick(results: Results) {
        Toast.makeText(context, results.name, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        Log.d(Tag, "onStart Called")
        super.onStart()
    }

    override fun onResume() {
        Log.d(Tag, "onResume Called")
        super.onResume()
    }

    override fun onPause() {
        Log.d(Tag, "onPause Called")
        super.onPause()
    }

    override fun onStop() {
        Log.d(Tag, "onStop Called")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(Tag, "onDestroy Called")
        super.onDestroy()
    }
}