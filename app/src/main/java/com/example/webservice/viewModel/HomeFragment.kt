package com.example.webservice.viewModel

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import androidx.paging.LoadState
import androidx.paging.map
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.webservice.R
import com.example.webservice.adapter.CustomAdapter
import com.example.webservice.adapter.RecyclerViewAdapter
import com.example.webservice.databinding.FragmentHomeBinding
import com.example.webservice.model.Credit
import com.example.webservice.model.Results
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.flow.collectLatest
import retrofit2.Response

const val TAG = "HomeFragment"
class HomeFragment : Fragment(), RecyclerViewAdapter.ItemClickListener /*CustomAdapter.ItemClickListener*/ {

    private lateinit var binding: FragmentHomeBinding
    private val mainViewModel: MainViewModel by viewModels()
    //private lateinit var adapter : CustomAdapter
    private var characters: List<Results> = listOf()
    private var numberPage: Int = 0
    private lateinit var status: String

    lateinit var recyclerViewAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        arguments?.let { }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        characters = listOf()
        mainViewModel.resetData()
        Log.d(TAG, characters.toString())
        binding = FragmentHomeBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.mainViewModel = mainViewModel

        //loadData()

        initRecyclerView(binding.root)
        getPagingData()

        /*binding.fab.setOnClickListener {
            //Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_newFragment, null)
            val action =HomeFragmentDirections.actionHomeFragmentToNewFragment()
            findNavController().navigate(action)
        }*/

        binding.fab.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_newFragment, null)
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated")
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        //toolbar.setTitle("Inicio")
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        view.findViewById<Toolbar>(R.id.toolbar)
            .setupWithNavController(navController, appBarConfiguration)
        //super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        Log.d(com.example.webservice.TAG, "onStart Called")
        super.onStart()
    }

    override fun onResume() {
        Log.d(com.example.webservice.TAG, "onResume Called")
        super.onResume()
    }

    override fun onPause() {
        Log.d(com.example.webservice.TAG, "onPause Called")
        super.onPause()
    }

    override fun onStop() {
        Log.d(com.example.webservice.TAG, "onStop Called")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(TAG, "OnDestroy")
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_items, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.loadData -> {
                mainViewModel.resetData()
                characters = listOf()
                //adapter.setData(characters)
                //adapter.notifyDataSetChanged()

                getPagingData()
                //loadData()
                true
            }
            R.id.search_byGender -> {
                characters = listOf()
                //adapter.setData(characters)
                //adapter.notifyDataSetChanged()
                val action = HomeFragmentDirections.actionHomeFragmentToNewFragment()
                findNavController().navigate(action)
                true
            }
            R.id.search_byStatus -> {
                characters = listOf()
                //adapter.setData(characters)
                //adapter.notifyDataSetChanged()
                val action = HomeFragmentDirections.actionHomeFragmentToStatusFragment()
                findNavController().navigate(action)
                /*
                val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
                val view = layoutInflater.inflate(R.layout.layout_bottom_sheet, null)
                bottomSheetDialog.setContentView(view)
                bottomSheetDialog.show()
                view.findViewById<Button>(R.id.searchButton).setOnClickListener {
                    status = when (view.findViewById<RadioGroup>(R.id.status_options).checkedRadioButtonId){
                        R.id.alive_option -> "alive"
                        R.id.dead_option -> "dead"
                        else -> "unknown"
                    }
                    loadCharactersByStatus()
                    bottomSheetDialog.dismiss()
                }
                */
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initRecyclerView(view: View) {
        recyclerViewAdapter = RecyclerViewAdapter(this)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            //recyclerViewAdapter = RecyclerViewAdapter(this)
            //recyclerViewAdapter = RecyclerViewAdapter(this)
            adapter = recyclerViewAdapter
        }
        /*val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        adapter = CustomAdapter(characters, this)
        recyclerView.adapter = adapter*/
    }

    private fun getPagingData() {
        lifecycleScope.launchWhenCreated {
            mainViewModel.getListData().collectLatest {
                recyclerViewAdapter.submitData(it)
            }
        }
        recyclerViewAdapter.addLoadStateListener { loadState ->
            binding.progressBarPaging.isVisible = loadState.refresh is LoadState.Loading ||
                    loadState.append is LoadState.Loading
            if (loadState.refresh is LoadState.Error /*|| loadState.append is LoadState.Error*/) {
                Toast.makeText(context, "Algo salió mal...", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun loadData() {
        mainViewModel.loadCharacters(1)
        mainViewModel.getCharacters.observe(viewLifecycleOwner, Observer { response ->
            if (response != null) {
                characters =  response.body()!!.results
                //adapter.setData(characters)
                //Log.d(TAG, "Internet connection error")
                //Toast.makeText(context, "Total: ${response.body()!!.results.size}", Toast.LENGTH_SHORT).show()
                //binding.responseBody.text = "No data found."
                //binding.responseCode.text = "Error: 404"
                //Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            }
        })
        /*mainViewModel.message.observe(viewLifecycleOwner, Observer { message ->
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            Log.d(TAG, message.toString())
        })*/
    }

    private fun loadCharactersByStatus() {
        mainViewModel.loadCharactersByStatus(status)
        mainViewModel.getCharactersByStatus.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            } else {
                characters =  response.body()!!.results
                Log.d("Response", response.toString())
                //adapter.setData(characters)
            }
        })
    }

    override fun onItemClick(results: Results) {
        Toast.makeText(context, results.name, Toast.LENGTH_SHORT).show()
    }

    /*
    private fun getCreditById() {
        val editTextPost = binding.editTextPost
        val id = editTextPost.text.toString().toInt()
        mainViewModel.getCredit(id)
        mainViewModel._getCredit.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                binding.responseBody.text = "No data found."
                binding.responseCode.text = "Error: 404"
                Log.d("getCreditById", "nulo")
            } else {
                if (it.body()?.status == 200) {
                    Log.d("Operaciones", "no nulo")
                    Log.d("Operaciones", it.toString())
                    binding.responseBody.text = it.body()?.credit.toString()
                    binding.responseCode.text = it.code().toString()
                } else {
                    binding.responseBody.text = "No data found."
                    binding.responseCode.text = it.code().toString()
                }
            }

        })
        mainViewModel._messageCredit.observe(viewLifecycleOwner, Observer { message ->
            //Log.d("getCreditById", message)
            //binding.responseCode.text = message
        })
    }

    private fun createNewCredit() {
        val credit = Credit("CREADO", 10, 50000, 1, "2021/06/10", 1, 1)
        mainViewModel.createNewCredit(credit)
        mainViewModel._credit.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) {
                binding.responseBody.text = "Error to create new credit"
                binding.responseCode.text = "Error code: 404"
            } else {
                if (response.body()?.status == 201) {
                    binding.responseBody.text = response.body()?.credit.toString()
                    binding.responseCode.text = response.code().toString()
                    Toast.makeText(context, response.body()?.message, Toast.LENGTH_SHORT).show()
                } else {
                    binding.responseBody.text = "Bad Request."
                    binding.responseCode.text = response.code().toString()
                }
            }
        })
        mainViewModel._messageCredit.observe(viewLifecycleOwner, Observer { message ->
            Log.d("NewCredit", message)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            //binding.responseBody.text = message
        })
    }

    private fun updateCredit(){
        val editTextPost = binding.editTextPost
        val id = editTextPost.text.toString().toInt()
        val credit = Credit("CREADO Y MODIFICADO", 10, 50000, 1,"2021/06/10", 1, 1)
        mainViewModel.updateCredit(id, credit)
        mainViewModel.creditUpdated.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) {
                binding.responseBody.text = "Error to update credit"
                binding.responseCode.text = "Error code: 400"
            } else {
                if (response.body()?.status == 201) {
                    Log.d("Operaciones", response.body().toString())
                    binding.responseBody.text = response.body().toString()
                    binding.responseCode.text = response.code().toString()
                    //Toast.makeText(this, response.body()?.message, Toast.LENGTH_SHORT).show()
                } else {
                    binding.responseBody.text = "Bad Request."
                    binding.responseCode.text = response.code().toString()
                }
            }
        })
    }

    private fun deleteCredit() {
        val editTextPost = binding.editTextPost
        val id = editTextPost.text.toString().toInt()
        mainViewModel.deleteCredit(id)
        mainViewModel._deleteCredit.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) {
                binding.responseBody.text = "Error to delete credit"
                binding.responseCode.text = "Error code: 400"
            } else {
                if (response.body()?.status == 200) {
                    binding.responseBody.text = response.body()?.message
                    binding.responseCode.text = response.code().toString()
                } else {
                    binding.responseBody.text = response.message()
                    binding.responseCode.text = response.code().toString()
                }
            }
        })
        mainViewModel._messageCredit.observe(viewLifecycleOwner, Observer { message ->
            Log.d("getCreditById", message)
            //binding.responseCode.text = message
        })
    }
    */
}